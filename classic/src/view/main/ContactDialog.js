Ext.define('ContactList.view.main.ContactDialog', {
    extend: 'Ext.window.Window',
    xtype: 'contact-dlg',

    width: 500,
    height: 450,
    layout: 'fit',
    closeAction: 'hide',
    closable: false,
    bind: {
        title: '{title}'
    },
    items: [
        {
            xtype: 'form',
            reference: 'contactForm',
            margin: '10 10 10 10',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            defaults: {
                allowBlank: false
            },
            items: [
                {
                    xtype: 'textfield',
                    fieldLabel: 'Name',
                    bind: {
                        value: '{firstname}'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Surname',
                    bind: {
                        value: '{lastname}'
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Number',
                    bind: {
                        value: '{phone}'
                    },
                    validator: function (v) {
                        return (v && /^\d+$/.test(v)) || 'Value must be numeric';
                    }
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Mail',
                    vtype: 'email',
                    bind: {
                        value: '{email}'
                    }
                },
                {
                    xtype: 'form',
                    reference: 'imageForm',
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'filefield',
                            allowBlank: true,
                            itemId: 'imageField',
                            fieldLabel: 'Image',
                            buttonText: 'Browse..',
                            listeners: {
                                change: 'onImageFieldChange',
                                afterrender: 'onImageFieldAfterRender'
                            }
                        }
                    ]
                },
                {
                    xtype: 'image',
                    cls: 'imageDisplayCls',
                    alt: 'image',
                    id: 'contactDlgImageField',
                    bind: {
                        src: '{imageTmp}'
                    }
                }
            ],
            buttons: [
                {
                    text: 'Save',
                    formBind: true,
                    bind: {
                        handler: '{handler}'
                    }
                },
                {
                    text: 'Close',
                    handler: 'onCloseContactDialog'
                }
            ]
        }
    ]
});
