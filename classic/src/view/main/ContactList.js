Ext.define('ContactList.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'ContactList.store.Contact'
    ],

    store: {
        type: 'contact'
    },

    columns: [
        {text: 'Name', dataIndex: 'firstname', flex: 1},
        {text: 'Surname', dataIndex: 'lastname', flex: 1},
        {text: 'Number', dataIndex: 'phone', flex: 1},
        {text: 'Mail', dataIndex: 'email', flex: 1},
        {
            text: 'Image',
            dataIndex: 'image',
            flex: 1,
            align: 'center',
            menuDisabled: true,
            sortable: false,
            renderer: function (value) {
                return value ? '<img src="' + value + '" style=width:50px;height:50px;/>' : '';
            }
        }
    ],

    bind: {
        selection: '{selection}'
    },

    tbar: [
        {
            xtype: 'button',
            text: 'Add',
            handler: 'onOpenAddContactDialog'
        },
        {
            xtype: 'button',
            text: 'Edit',
            bind: {
                disabled: '{!selection}'
            },
            handler: 'onOpenEditContactDialog'
        },
        {
            xtype: 'button',
            text: 'Delete',
            bind: {
                disabled: '{!selection}'
            },
            handler: 'onDeleteContact'
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'button',
            text: 'Refresh',
            handler: 'onReload'
        },
        {
            xtype: 'tbseparator'
        },
        {
            xtype: 'textfield',
            emptyText: 'Search',
            enableKeyEvents: true,
            listeners: {
                keypress: 'onContactSearch'
            }
        }
    ]
});
