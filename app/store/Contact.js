Ext.define('ContactList.store.Contact', {
    extend: 'Ext.data.Store',

    alias: 'store.contact',
    storeId: 'contact',

    requires: [
        'ContactList.model.Contact'
    ],

    model: 'ContactList.model.Contact',

    autoLoad: true,
    remoteFilter: true,

    proxy: {
        type: 'ajax',
        url: 'http://contactlistservice.getsandbox.com/contact',
        actionMethods: {
            update: 'PUT',
            destroy: 'DELETE'
        },
        reader: {
            type: 'json'
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            allowSingle: true
        }
    }
});
