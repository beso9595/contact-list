Ext.define('ContactList.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        selection: null,
        title: null,
        handler: null
    }
});
