Ext.define('ContactList.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    onOpenAddContactDialog: function () {
        var me = this;
        var vm = me.getViewModel();
        var dlg = me.lookupReference('contactDlg');
        vm.set('title', 'Add');
        vm.set('handler', 'onAddContact');
        vm.set('imageTmp', null);
        vm.set('imageField', null);

        me.lookupReference('contactForm').reset();
        me.getView().mask();
        dlg.show().center();
    },

    onOpenEditContactDialog: function () {
        var me = this;
        var vm = me.getViewModel();
        var dlg = me.lookupReference('contactDlg');
        vm.set('handler', 'onEditContact');

        var selection = vm.get('selection');
        vm.set('title', 'Edit: ' + selection.get('firstname') + ' ' + selection.get('lastname'));
        vm.set('firstname', selection.get('firstname'));
        vm.set('lastname', selection.get('lastname'));
        vm.set('phone', selection.get('phone'));
        vm.set('email', selection.get('email'));

        me.lookupReference('imageForm').getComponent('imageField').setRawValue(null);
        if (document.getElementById('contactDlgImageField')) {
            document.getElementById('contactDlgImageField').src = selection.get('image');
        } else {
            vm.set('imageTmp', selection.get('image'));
        }
        me.getView().mask()
        dlg.show().center();
    },

    onDeleteContact: function () {
        var vm = this.getViewModel();
        var selection = vm.get('selection');

        Ext.Msg.show({
            title: 'Delete record',
            message: 'Do you want to delete record: <b>' + selection.get('firstname') + ' ' + selection.get('lastname') + '</b>',
            closable: false,
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.QUESTION,
            fn: function (btn) {
                if (btn === 'yes') {
                    var store = Ext.getStore('contact');
                    store.remove(selection);
                    store.sync({
                        success: function () {
                            Ext.toast('Successfully deleted');
                        },
                        failure: function () {
                            store.rejectChanges();
                            Ext.toast('Error while deleting');
                        }
                    });
                }
            },
            scope: this
        });
    },

    onReload: function () {
        var store = Ext.getStore('contact');
        if (store.getFilters().length > 0) {
            store.clearFilter()
        } else {
            store.reload();
        }
    },

    onContactSearch: function (cmp, e) {
        if (e.browserEvent.key === 'Enter') {
            var value = cmp.getValue();
            var store = Ext.getStore('contact');
            if (value) {
                store.filter([
                    'firstname',
                    'lastname',
                    'phone',
                    'email'
                ].map(function (e) {
                    return {
                        property: e,
                        value: value
                    };
                }));
            } else {
                store.clearFilter();
            }
        }
    },

    onImageFieldChange: function (cmp) {
        var file = cmp.getEl().down('input[type=file]').dom.files[0];
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            document.getElementById('contactDlgImageField').src = reader.result;
        };
    },

    onImageFieldAfterRender: function (cmp) {
        cmp.fileInputEl.set({
            accept: 'image/*'
        });
    },

    onAddContact: function (btn) {
        var me = this;
        var vm = me.getViewModel();
        var store = Ext.getStore('contact');
        store.add({
            firstname: vm.get('firstname'),
            lastname: vm.get('lastname'),
            phone: vm.get('phone'),
            email: vm.get('email'),
            image: document.getElementById('contactDlgImageField').src
        });
        store.sync({
            success: function () {
                store.reload();
                Ext.toast('Successfully added');
            },
            failure: function () {
                store.rejectChanges();
                Ext.toast('Error while adding');
            }
        });
        me.onCloseContactDialog(btn);
    },

    onEditContact: function (btn) {
        var me = this;
        var vm = me.getViewModel();
        var store = Ext.getStore('contact');
        var selection = vm.get('selection');
        selection.set('firstname', vm.get('firstname'));
        selection.set('lastname', vm.get('lastname'));
        selection.set('phone', vm.get('phone'));
        selection.set('email', vm.get('email'));
        selection.set('image', document.getElementById('contactDlgImageField').src);
        store.sync({
            success: function () {
                Ext.toast('Successfully updated');
            },
            failure: function () {
                store.rejectChanges();
                Ext.toast('Error while updating');
            }
        });
        me.onCloseContactDialog(btn);
    },

    onCloseContactDialog: function (btn) {
        this.getView().unmask();
        btn.up('window').close();
    }
});
