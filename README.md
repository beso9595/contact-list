# Contact List

* clone
* run `sencha app watch` in project root directory
* go to `localhost:1841`

`required:`
* `Sencha Cmd 6.2` or higher

### Screenshots:

![load.jpg](https://i.imgur.com/l6smN8l.png)

![edit.jpg](https://i.imgur.com/JS4QzZI.png)

![delete.jpg](https://i.imgur.com/N6W7wkG.png)

![search.jpg](https://i.imgur.com/dgrgw19.png)